package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */
public class VideoList {
    private String previousToken;
    private String nextToken;
    private int resultsPerPage;
    private int totalResults;
    private List<Video> videos;

    public VideoList(String previousToken, String nextToken, int resultsPerPage, int totalResults) {
        this.previousToken = previousToken;
        this.nextToken = nextToken;
        this.resultsPerPage = resultsPerPage;
        this.totalResults = totalResults;
        this.videos = new ArrayList<Video>();
    }

    public void addVideo(Video v) {
        videos.add(v);
    }

    public void removeVideo(Video v) {
        videos.remove(v);
    }

    public Video getVideo(int index) {
        return videos.get(index);
    }

    public int getVideoListSize() {
        return videos.size();
    }

    public void removeAllVideos() {
       videos.removeAll(videos);
    }

    public String getPreviousToken() {
        return previousToken;
    }

    public String getNextToken() {
        return nextToken;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setPreviousToken(String previousToken) {
        this.previousToken = previousToken;
    }

    public void setNextToken(String nextToken) {
        this.nextToken = nextToken;
    }

    public void setResultsPerPage(int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }


}
