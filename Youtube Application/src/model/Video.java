package model;



import model.api_data_model.video_details.Thumbnails;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
public class Video {

    private String videoId;
    private Date publishedAt;
    private String title;
    private String description;
    private Thumbnails thumbnails;
    private String duration;
    private String uploadStatus;
    private int viewCount;
    private int likeCount;
    private int dislikeCount;
    private int commentCount;

    public Video(String videoId, Date publishedAt, String title, String description, Thumbnails thumbnails, String duration, String uploadStatus, int viewCount, int likeCount, int dislikeCount, int commentCount) {
        this.videoId = videoId;
        this.publishedAt = publishedAt;
        this.title = title;
        this.description = description;
        this.thumbnails = thumbnails;
        this.duration = duration;
        this.uploadStatus = uploadStatus;
        this.viewCount = viewCount;
        this.likeCount = likeCount;
        this.dislikeCount = dislikeCount;
        this.commentCount = commentCount;
    }

    public String getVideoId() {
        return videoId;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    public String getDuration() {
        return duration;
    }

    public String getUploadStatus() {
        return uploadStatus;
    }

    public int getViewCount() {
        return viewCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }
}

