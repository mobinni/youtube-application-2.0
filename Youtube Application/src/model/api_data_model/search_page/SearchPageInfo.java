package model.api_data_model.search_page;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class SearchPageInfo {
    private int totalResults;
    private int resultsPerPage;

    public int getTotalResults() {
        return totalResults;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }
}
