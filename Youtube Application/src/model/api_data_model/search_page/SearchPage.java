package model.api_data_model.search_page;

import com.google.gson.annotations.SerializedName;
import model.api_data_model.video_details.VideoPage;
import model.api_data_model.video_details.VideoPageItem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
public class SearchPage {


    @SerializedName("pageInfo")
    private SearchPageInfo pageInfo;

    private String nextPageToken;
    private String prevPageToken;

    @SerializedName("items")
    private LinkedList<SearchPageItem> searchPageItems;
    public SearchPage() {

    }



    public String getNextPageToken() {
        return nextPageToken;
    }

    public String getPrevPageToken() {
        return prevPageToken;
    }

    public SearchPageInfo getPageInfo() {
        return pageInfo;
    }

    public List<SearchPageItem> getSearchPageItems() {
        return searchPageItems;
    }
}
