package model.api_data_model.video_details;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */
public class VideoPageStatistics {
    private String viewCount;
    private String likeCount;
    private String dislikeCount;
    private String commentCount;

    public String getViewCount() {
        return viewCount;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public String getDislikeCount() {
        return dislikeCount;
    }

    public String getCommentCount() {
        return commentCount;
    }
}
