package model.api_data_model.video_details;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 19:05
 * To change this template use File | Settings | File Templates.
 */
public class VideoPageSnippet {
    private String publishedAt;
    private String channelId;
    private String title;
    private String description;
    private Thumbnails thumbnails;

    public String getPublishedAt() {
        return publishedAt;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }
}
