package model.api_data_model.video_details;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 19:02
 * To change this template use File | Settings | File Templates.
 */
public class VideoPage {
    @SerializedName("items")
    private ArrayList<VideoPageItem> items;



    public List<VideoPageItem> getItems() {
        return items;
    }


}
