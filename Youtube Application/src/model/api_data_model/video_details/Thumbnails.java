package model.api_data_model.video_details;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class Thumbnails {
    private Url normal;
    private Url medium;
    private Url high;
    private Url standard;
    private Url maxres;

    public Url getNormal() {
        return normal;
    }

    public Url getMedium() {
        return medium;
    }

    public Url getHigh() {
        return high;
    }

    public Url getStandard() {
        return standard;
    }

    public Url getMaxres() {
        return maxres;
    }
}
