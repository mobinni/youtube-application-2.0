package model.api_data_model.video_details;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 18:54
 * To change this template use File | Settings | File Templates.
 */
public class VideoPageItem {
    private String id;
    private VideoPageSnippet snippet;
    private VideoPageContentDetails contentDetails;
    private VideoPageStatus status;
    private VideoPageStatistics statistics;
    public VideoPageItem() {
    }

    public String getId() {
        return id;
    }

    public VideoPageSnippet getSnippet() {
        return snippet;
    }

    public VideoPageContentDetails getContentDetails() {
        return contentDetails;
    }


    public VideoPageStatus getVideoStatus() {
        return status;
    }

    public VideoPageStatistics getStatistics() {
        return statistics;
    }
}
