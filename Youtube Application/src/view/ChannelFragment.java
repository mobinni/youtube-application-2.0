package view;

import android.app.Fragment;
import android.app.ListFragment;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mo.binni.youtube.application.R;
import model.Utils;
import model.Video;
import model.VideoList;
import model.api_data_model.search_page.SearchPage;
import view.adapters.ChannelItemAdapter;
import view.data.ImageFetcher;
import view.datawrappers.ChannelItem;
import view.menu.adapters.LeftMenuAdapter;
import view.menu.datawrappers.LeftMenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 4/07/13
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
public class ChannelFragment extends Fragment {
    private VideoList videoList;
    public ChannelFragment(VideoList videoList) {
        this.videoList = videoList;
    }

    public ChannelFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.channel, container, false);
       this.setRetainInstance(true);
        ListView ls = (ListView) rootView.findViewById(R.id.channel);

        List<ChannelItem> items = new ArrayList<ChannelItem>();
        for(Video v : videoList.getVideos()) {
            items.add(new ChannelItem(v.getThumbnails().getMedium().getUrl(), v.getTitle(),v.getPublishedAt(),String.valueOf(v.getViewCount())));
        }
        ChannelItemAdapter item_adapter = new ChannelItemAdapter(this.getActivity(), R.layout.channel_list_item, items);
        ls.setAdapter(item_adapter);

        return rootView;
    }


}
