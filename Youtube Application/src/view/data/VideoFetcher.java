package view.data;

import android.os.AsyncTask;
import android.util.Log;
import api.Api;
import com.google.gson.Gson;
import model.Utils;
import model.Video;
import model.api_data_model.search_page.SearchPage;
import model.api_data_model.video_details.VideoPage;
import model.api_data_model.video_details.VideoPageItem;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 18:55
 * To change this template use File | Settings | File Templates.
 */
public class VideoFetcher {
    private Gson gson;
    private InputStream source;
    private Reader reader;
    private VideoPage videoPage;


    protected VideoPage retrieveVideoInformation(String videoId) {
        String url = buildVideoUrl(videoId);
        source = Utils.retrieveStream(getClass(), url);
        gson = new Gson();
        reader = new InputStreamReader(source);
        videoPage = gson.fromJson(reader, VideoPage.class);
        return videoPage;
    }


    private String buildVideoUrl(String id) {
        StringBuilder builder = new StringBuilder();
        builder.append("https://www.googleapis.com/youtube/v3/videos?id=");
        builder.append(id);
        builder.append("&key=");
        builder.append(Api.DEV_KEY);
        builder.append("&key=AIzaSyBh8EFaV8d_JYEBH-Fz__YLInjgbr2Z7fg&part=snippet,contentDetails,statistics,status");
        Log.i("URL VideoItem", String.valueOf(builder.toString()));
        return builder.toString();
    }

}
