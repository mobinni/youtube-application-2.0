package view.data;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.util.Log;
import api.Api;
import com.google.gson.Gson;
import com.mo.binni.youtube.application.R;
import model.Utils;
import model.Video;
import model.VideoList;
import model.api_data_model.search_page.SearchPage;
import model.api_data_model.search_page.SearchPageItem;
import model.api_data_model.video_details.VideoPage;
import model.api_data_model.video_details.VideoPageItem;
import view.ChannelFragment;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 2/07/13
 * Time: 13:22
 * To change this template use File | Settings | File Templates.
 */
public class PageFetcher extends AsyncTask<String, Integer, VideoList> {
    private Gson gson;
    private ProgressDialog pd;
    private InputStream source;
    private Reader reader;
    private SearchPage searchPage;
    private VideoFetcher fetcher;
    private Context context;
    private int myProgress = 0;
    private FragmentManager manager;



    public PageFetcher(Context context, FragmentManager manager) {
        this.manager = manager;
        fetcher = new VideoFetcher();
        pd = new ProgressDialog(context);
        this.context = context;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        pd.setMessage("Loading...");
        pd.show();
        pd.setCancelable(false);

    }

    @Override
    protected VideoList doInBackground(String... params) {
        String url = buildSearchUrl(params[0], params[1], params[2]);
        source = Utils.retrieveStream(getClass(), url);
        gson = new Gson();
        reader = new InputStreamReader(source);
        searchPage = gson.fromJson(reader, SearchPage.class);
        return convertPageToVideoList(searchPage);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);    //To change body of overridden methods use File | Settings | File Templates.
        Log.e("progress", values[0] +"");
        pd.setMessage("Loading... " + values[0] + "%");
    }

    @Override
    protected void onPostExecute(VideoList videoList) {
        pd.cancel();

        Fragment fragment = new ChannelFragment(videoList);
        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

    }

    private VideoList convertPageToVideoList(SearchPage searchPage) {

        VideoList videoList = new VideoList(searchPage.getPrevPageToken(), searchPage.getNextPageToken(),
                searchPage.getPageInfo().getResultsPerPage(), searchPage.getPageInfo().getTotalResults());
        for (SearchPageItem p : searchPage.getSearchPageItems()) {
            VideoPage page = fetcher.retrieveVideoInformation(p.getId().getVideoId());
            VideoPageItem item = page.getItems().get(0); // Get first and only item
            publishProgress(myProgress++ * 4);

            Video v = new Video(
                    item.getId(),
                    Utils.convertVideoDate(item.getSnippet().getPublishedAt()),
                    item.getSnippet().getTitle(),
                    item.getSnippet().getDescription(),
                    item.getSnippet().getThumbnails(),
                    item.getContentDetails().getDuration(),
                    item.getVideoStatus().getUploadStatus(),
                    Integer.parseInt(item.getStatistics().getViewCount()),
                    Integer.parseInt(item.getStatistics().getLikeCount()),
                    Integer.parseInt(item.getStatistics().getDislikeCount()),
                    Integer.parseInt(item.getStatistics().getCommentCount())
            );

            // add video to list
            videoList.addVideo(v);
        }
        return videoList;
    }


    private String buildSearchUrl(String query, String channelId, String pageToken) {
        StringBuilder builder = new StringBuilder();
        builder.append("https://www.googleapis.com/youtube/v3/search?q=");
        builder.append(query);
        builder.append("&key=");
        builder.append(Api.DEV_KEY);
        builder.append("&part=snippet&order=date&type=video&channelId=");
        builder.append(channelId);
        builder.append("&maxResults=25&pageToken=");
        builder.append(pageToken);
        Log.i("URL", String.valueOf(builder.toString()));
        return builder.toString();  //To change body of created methods use File | Settings | File Templates.
    }

}
