package view.datawrappers;

import android.graphics.drawable.Drawable;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 4/07/13
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */
public class ChannelItem {
    private String icon;
    private String title;
    private Date publishedAt;
    private String viewCount;

    public ChannelItem(String icon, String title, Date publishedAt, String viewCount) {
        this.icon = icon;
        this.title = title;
        this.publishedAt = publishedAt;
        this.viewCount = viewCount;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public String getViewCount() {
        return viewCount;
    }
}
