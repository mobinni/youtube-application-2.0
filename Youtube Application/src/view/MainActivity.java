package view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.mo.binni.youtube.application.R;
import model.VideoList;
import view.data.PageFetcher;
import view.menu.LeftMenu;

import java.util.concurrent.ExecutionException;


/*
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 15/06/13
 * Time: 22:48
 */
public class MainActivity extends FragmentActivity {

    private LeftMenu left_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        /* Initiate left menu helper class that contains the logic behind the menu and fires the necessary methods to create the menu
           and handle events */
        left_menu = new LeftMenu(
                this,
                getActionBar(),
                (ListView) findViewById(R.id.left_drawer),
                (DrawerLayout) findViewById(R.id.drawer_layout),
                this.getFragmentManager()
        );

        left_menu.setLeftMenuItems();   // create and add left menu items
        left_menu.setLeftMenuActionBarActions();   //  set drawer open/close indicator and eventhandling

    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        left_menu.getDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        left_menu.getDrawerToggle().onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /* Pass event to drawerToggle, if you pressed on the drawerToggle image you return true and fire the method that handles that */
        if (left_menu.getDrawerToggle().onOptionsItemSelected(item)) {
            return true;
        }
        // Handle slide
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // inflate top menu with action bar menu items (refresh button, ...)
       MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.actionbar_menu, menu);
       return true;
    }





}

