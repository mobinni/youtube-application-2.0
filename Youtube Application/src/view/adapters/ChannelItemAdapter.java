package view.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mo.binni.youtube.application.R;
import view.data.ImageFetcher;
import view.datawrappers.ChannelItem;
import view.menu.datawrappers.LeftMenuItem;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 4/07/13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
public class ChannelItemAdapter extends ArrayAdapter<ChannelItem> {
    private int layoutResourceId;
    private List<ChannelItem> data = null;
    ImageFetcher fetcher;

    public ChannelItemAdapter(Context context, int resource, List<ChannelItem> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.data = objects;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ItemHolder holder = new ItemHolder();

        if(convertView == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

        }

        holder.icon = (ImageView) row.findViewById(R.id.itemIcon);
        holder.title = (TextView) row.findViewById(R.id.title);
        holder.publishedAt = (TextView) row.findViewById(R.id.publishedAt);
        holder.viewCount = (TextView) row.findViewById(R.id.viewCount);

        Log.e("image", data.get(position).getIcon());
        fetcher = new ImageFetcher(holder.icon);
        fetcher.execute(data.get(position).getIcon());
        holder.title.setText(data.get(position).getTitle());
        holder.viewCount.setText(data.get(position).getViewCount());
        holder.publishedAt.setText(data.get(position).getPublishedAt().toString());


        return row;


    }

    static class ItemHolder {
        ImageView icon;
        TextView title;
        TextView publishedAt;
        TextView viewCount;

    }
}
