package view.menu;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.mo.binni.youtube.application.R;

import view.data.PageFetcher;
import view.menu.datawrappers.LeftMenuItem;
import view.menu.adapters.LeftMenuAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.support.v4.app.ActivityCompat.invalidateOptionsMenu;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 1/07/13
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */
public class LeftMenu {
    private Context context;
    private ActionBar actionBar;
    private ListView left_menu;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle drawerToggle;
    private FragmentManager manager;
    private String title;

    public LeftMenu(Context context, ActionBar actionBar, ListView left_menu, DrawerLayout drawer, FragmentManager manager) {
        this.context = context;
        this.actionBar = actionBar;
        this.left_menu = left_menu;
        this.drawer = drawer;
        this.manager = manager;
        title = context.getString(R.string.app_name);
    }

    public void setLeftMenuItems() {
        // Shows
        List<LeftMenuItem> menu_items = new ArrayList<LeftMenuItem>();
        menu_items.add(new LeftMenuItem("Shows", -1));
        menu_items.add(new LeftMenuItem("The PhillyD Show", R.drawable.sxephil_icon));
        menu_items.add(new LeftMenuItem("Sourcefed", R.drawable.sourcefed_icon));
        menu_items.add(new LeftMenuItem("Sourcefed Nerd", R.drawable.sourcefed_nerd_icon));

        // Vlogs
        menu_items.add(new LeftMenuItem("Vlogs", -1));
        menu_items.add(new LeftMenuItem("The Vloggity", R.drawable.phillyd_icon));
        menu_items.add(new LeftMenuItem("Lee Newton", R.drawable.lee_icon));
        menu_items.add(new LeftMenuItem("Elliot Morgan", R.drawable.elliot_icon));
        menu_items.add(new LeftMenuItem("Steve Zaragoza", R.drawable.steve_icon));
        menu_items.add(new LeftMenuItem("Meg Turney", R.drawable.meg_icon));

        // Other
        menu_items.add(new LeftMenuItem("Other", -1));
        menu_items.add(new LeftMenuItem("For Human Peoples", R.drawable.fhp_icon));
        menu_items.add(new LeftMenuItem("The Forum", R.drawable.sxephil_icon));

        LeftMenuAdapter menu_adapter = new LeftMenuAdapter(context, R.layout.left_menu_item, menu_items);
        left_menu.setAdapter(menu_adapter);
        left_menu.setOnItemClickListener(new DrawerItemClickListener());

    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                selectItem(position);
            } catch (ExecutionException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void setLeftMenuActionBarActions() {
        // enable ActionBar app icon to behave as action to toggle nav drawer
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        drawerToggle = new ActionBarDrawerToggle(
                (Activity) context,                  /* host Activity */
                drawer,         /* DrawerLayout object */
                R.drawable.menu_slider_icon,  /* nav drawer image to replace 'Up' caret */
                R.string.md__drawerOpenIndicatorDesc,  /* "open drawer" description for accessibility */
                R.string.md__drawerClosedIndicatorDesc  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                actionBar.setTitle(title);
                invalidateOptionsMenu((Activity) context);
            }

            public void onDrawerOpened(View drawerView) {
                actionBar.setTitle(context.getString(R.string.navigation));
                invalidateOptionsMenu((Activity) context); // creates call to onPrepareOptionsMenu()
            }
        };
        drawer.setDrawerListener(drawerToggle);


    }

    public void selectItem(int position) throws ExecutionException, InterruptedException {
       // update the main content by replacing fragments
        PageFetcher p = new PageFetcher(context, manager);
        String[] data = channelData(position);
        p.execute(data[0], data[1], "");


        title =  ((LeftMenuItem) left_menu.getItemAtPosition(position)).getTitle();

        // update selected item and title, then close the drawer
        left_menu.setItemChecked(position, true);
        drawer.closeDrawer(left_menu);
    }

    private String[] channelData(int position) {
       switch (position) {
           case 0: return new String[]{"sxephil", "UClFSU9_bUb4Rc6OYfTt5SPw"};
           case 1: return new String[]{"sourcefed", "UC_gE-kg7JvuwCNlbZ1-shlA"};
           case 2: return new String[]{"sourcefed+nerd", "UCN8XYDRZRzhKg3r8qBR35FA"};
           default: return  new String[]{"sxephil", "UClFSU9_bUb4Rc6OYfTt5SPw"};
       }
    }


    public ActionBarDrawerToggle getDrawerToggle() {
        return drawerToggle;
    }

}
