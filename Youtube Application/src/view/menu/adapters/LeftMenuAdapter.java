package view.menu.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mo.binni.youtube.application.R;
import view.menu.datawrappers.LeftMenuItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 22/06/13
 * Time: 2:04
 * To change this template use File | Settings | File Templates.
 */
public class LeftMenuAdapter extends ArrayAdapter<LeftMenuItem> {
    private static final Integer LIST_HEADER = 0;
    private static final Integer LIST_ITEM = 1;

    private int layoutResourceId;
    private List<LeftMenuItem> data = null;



    public LeftMenuAdapter(Context context, int resource, List<LeftMenuItem> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        boolean headerText = getHeader(position);

        if(headerText) {
            View item = convertView;
            if(convertView == null || convertView.getTag() == LIST_ITEM) {

                item = LayoutInflater.from(getContext()).inflate(
                        R.layout.menu_header_layout, parent, false);

                item.setTag(LIST_HEADER);
            }

            TextView headerTextView = (TextView)item.findViewById(R.id.list_hdr);
            headerTextView.setText(data.get(position).getTitle());
            return item;
        }

        View row = convertView;
        ItemHolder holder;

            if(convertView == null || convertView.getTag() == LIST_HEADER) {
                LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

            }

        holder = new ItemHolder();
        holder.icon = (ImageView) row.findViewById(R.id.iconImage);
        holder.text = (TextView) row.findViewById(R.id.iconText);

        row.setTag(LIST_ITEM);

        holder.text.setText(data.get(position).getTitle());
        holder.icon.setImageResource(data.get(position).getIcon());


        return row;
    }

    // get position in list of header
    private boolean getHeader(int position) {
        switch (position) {
            case 0:
                return true;
            case 4:
                return true;
            case 10:
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
       return !getHeader(position);
    }

    static class ItemHolder {
        ImageView icon;
        TextView text;
    }
}
