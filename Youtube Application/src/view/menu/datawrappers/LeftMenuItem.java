package view.menu.datawrappers;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 22/06/13
 * Time: 2:15
 * To change this template use File | Settings | File Templates.
 */
public class LeftMenuItem {
    private int icon;
    private String title;

    public LeftMenuItem(String title, int icon) {
        super();
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
